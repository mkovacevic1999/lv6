﻿using System;
using System.Windows.Forms;

//Napravite aplikaciju znanstveni kalkulator koja će imati funkcionalnost
//znanstvenog kalkulatora, odnosno implementirati osnovne(+,-,*,/) i barem 5
//naprednih(sin, cos, log, sqrt...) operacija.

namespace WindowsFormsApp1 {
	public partial class Form1 : Form {
		private string operacija="";
		private string drugeoperacije;
		private double op1, op2, rez;
		private bool isPerformed;
		public Form1() => InitializeComponent();

		private void Form1_Load(object sender, EventArgs e) {
		}

		private void btn_broj_Click(object sender, EventArgs e) {
			Button triggerButton = (Button)sender;
			if (isPerformed) {
				textBox1.Text = "";
				isPerformed = false;
			}
			if (triggerButton.Text == ",") {
				if (!textBox1.Text.Contains(",")) {
					textBox1.Text = textBox1.Text + triggerButton.Text;
				}
			} else {
				textBox1.Text = textBox1.Text + triggerButton.Text;
			}
		}

		private void btn_minus_Click(object sender, EventArgs e) {
			operacija = "-";
			PretvoriBrojOp1SakrijDugmad();
		}

		private void btn_mnozenje_Click(object sender, EventArgs e) {
			operacija = "*";
			PretvoriBrojOp1SakrijDugmad();
		}

		private void PretvoriBrojOp1SakrijDugmad() {
			try {
				op1 = Convert.ToDouble(textBox1.Text);
				SakrijDugmad();
			} catch (Exception) {
				MessageBox.Show("Pogresan unos operanda", "Greska");
			}
			textBox1.Text = "";
		}

		private void SakrijDugmad() {
			btn_plus.Hide();
			btn_minus.Hide();
			btn_dijeljenje.Hide();
			btn_mnozenje.Hide();
			btn_log.Hide();
			btn_sin.Hide();
			btn_cos.Hide();
			btn_sqrt.Hide();
			btn_tan.Hide();
			btn_pow.Hide();
		}

		private void btn_dijeljenje_Click(object sender, EventArgs e) {
			operacija = "/";
			PretvoriBrojOp1SakrijDugmad();

		}

		private void btn_jednako_Click(object sender, EventArgs e) {
			if (textBox1.Text == "") {
				return;
			}

			isPerformed = true;
			if (operacija == "sin" || operacija == "cos" || operacija == "tan" || operacija == "log" || operacija == "sqrt" || operacija == "pow") {
				switch (operacija) {
					case "sin":
						drugeoperacije = textBox1.Text;
						try {
							op1 = Convert.ToDouble(drugeoperacije.Substring(3));
							rez = Math.Sin(op1);
						} catch (Exception) {
							MessageBox.Show("Pogresan unos operanda", "Greska");
						}

						break;
					case "log":
						drugeoperacije = textBox1.Text;
						try {
							op1 = Convert.ToDouble(drugeoperacije.Substring(3));
							rez = Math.Log10(op1);
						} catch (Exception) {
							MessageBox.Show("Pogresan unos operanda", "Greska");
						}

						break;
					case "cos":
						drugeoperacije = textBox1.Text;
						try {
							op1 = Convert.ToDouble(drugeoperacije.Substring(3));
							rez = Math.Cos(op1);
						} catch (Exception) {
							MessageBox.Show("Pogresan unos operanda", "Greska");

						}

						break;
					case "tan":
						drugeoperacije = textBox1.Text;
						try {
							op1 = Convert.ToDouble(drugeoperacije.Substring(3));
							rez = Math.Tan(op1);
						} catch (Exception) {
							MessageBox.Show("Pogresan unos operanda", "Greska");
						}

						break;
					case "sqrt":
						drugeoperacije = textBox1.Text;
						try {
							op1 = Convert.ToDouble(drugeoperacije.Substring(1));
							rez = Math.Sqrt(op1);
						} catch (Exception) {
							MessageBox.Show("Pogresan unos operanda", "Greska");
						}

						break;
					case "pow":
						rez = Math.Pow(op1, 2);
						break;
				}
				textBox1.Text = rez.ToString();
			} else {
				try {
					op2 = Convert.ToDouble(textBox1.Text);
				} catch (Exception) {
					MessageBox.Show("Pogresan unos operanda", "Greska");
				}
				switch (operacija) {
					case "+":
						rez = op1 + op2;
						break;
					case "-":
						rez = op1 - op2;
						break;
					case "*":
						rez = op1 * op2;
						break;
					case "/":
						rez = op1 / op2;
						break;
					case "":
						rez = Convert.ToDouble(textBox1.Text);
						break;
				}
				textBox1.Text = rez.ToString();
			}
			OtkrijDugmad();
			operacija = "";
		}

		private void OtkrijDugmad() {
			btn_plus.Show();
			btn_minus.Show();
			btn_dijeljenje.Show();
			btn_mnozenje.Show();
			btn_log.Show();
			btn_sin.Show();
			btn_cos.Show();
			btn_sqrt.Show();
			btn_tan.Show();
			btn_pow.Show();
		}

		private void btn_Quit_Click(object sender, EventArgs e) => Application.Exit();

		private void btn_sin_Click(object sender, EventArgs e) {
			if (isPerformed == true) {
				textBox1.Text = "";
				isPerformed = false;
			}
			operacija = "sin";
			textBox1.Text = textBox1.Text + "sin";
			SakrijDugmad();
		}

		private void btn_sqrt_Click(object sender, EventArgs e) {
			if (isPerformed == true) {
				textBox1.Text = "";
				isPerformed = false;
			}
			operacija = "sqrt";
			textBox1.Text = textBox1.Text + "√";
			SakrijDugmad();
		}

		private void btn_cos_Click(object sender, EventArgs e) {
			if (isPerformed == true) {
				textBox1.Text = "";
				isPerformed = false;
			}
			operacija = "cos";
			textBox1.Text = textBox1.Text + "cos";
			SakrijDugmad();
		}
		private void btn_tan_Click(object sender, EventArgs e) {
			if (isPerformed == true) {
				textBox1.Text = "";
				isPerformed = false;
			}
			operacija = "tan";
			textBox1.Text = textBox1.Text + "tan";
			SakrijDugmad();
		}

		private void btn_log_Click(object sender, EventArgs e) {
			if (isPerformed == true) {
				textBox1.Text = "";
				isPerformed = false;
			}
			operacija = "log";
			textBox1.Text = textBox1.Text + "log";
			SakrijDugmad();

		}

		private void btn_pow_Click(object sender, EventArgs e) {
			operacija = "pow";

			textBox1.Text = textBox1.Text + "^2";
			try {
				op1 = Convert.ToDouble(textBox1.Text.Substring(0, textBox1.Text.Length - 2));
				SakrijDugmad();

			} catch (Exception) {
				MessageBox.Show("Pogresan unos operanda", "Greska");
				textBox1.Text = "";
			}
		}

		private void btn_promjeni_predznak_Click(object sender, EventArgs e) {
			string br = "";
			if (textBox1.Text == "") {
				return;
			}

			switch (operacija) {
				case "pow":
					br = textBox1.Text.Replace("^2", "");
					break;
				case "log":
					br = textBox1.Text.Replace("log", "");
					break;
				case "sin":
					br = textBox1.Text.Replace("sin", "");
					break;
				case "cos":
					br = textBox1.Text.Replace("cos", "");
					break;
				case "tan":
					br = textBox1.Text.Replace("tan", "");
					break;
				case "sqrt":
					br = textBox1.Text.Replace("√", "");
					break;
				default:
					br = textBox1.Text;
					break;

			}
			double novi_broj = Convert.ToDouble(br) * (-1);
			textBox1.Text = textBox1.Text.Replace(br, novi_broj.ToString());
		}

		private void btn_clear_Click(object sender, EventArgs e) {
			isPerformed = false;
			op1 = 0;
			op2 = 0;
			rez = 0;
			OtkrijDugmad();
			textBox1.Text = "";
		}

		private void btn_plus_Click(object sender, EventArgs e) {
			operacija = "+";
			try {
				op1 = Convert.ToDouble(textBox1.Text);
				SakrijDugmad();
			} catch (Exception) {
				MessageBox.Show("Pogresan unos operanda", "Greska");
			}
			textBox1.Text = "";
		}
	}
}
