﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;


//Napravite jednostavnu igru vješala.Pojmovi se učitavaju u listu iz datoteke, i u
//svakoj partiji se odabire nasumični pojam iz liste. Omogućiti svu
//funkcionalnost koju biste očekivali od takve igre.Nije nužno crtati vješala,
//dovoljno je na labeli ispisati koliko je pokušaja za odabir slova preostalo. 
namespace Vješala {
	public partial class Form1 : Form {
		private string pojam = "";
		private int count = 0;
		private Random rand1 = new Random();
		private List<string> lista = new List<string>();
		private string path = "pojmovi.txt";

		public Form1() => InitializeComponent();

		private void Form1_Load(object sender, EventArgs e) {

			btn_UnosSlova.Enabled = false;
			btnReset.Enabled = false;
			lblBroj.Text = "";
		}

		private void btnIzlaz_Click(object sender, EventArgs e) => Application.Exit();

		private void btnIgraj_Click(object sender, EventArgs e) {
			lblBroj.Text = "3";
			btn_UnosSlova.Enabled = true;
			btnReset.Enabled = true;

			using (StreamReader reader = new StreamReader(@path)) {
				string line;
				while ((line = reader.ReadLine()) != null) {
					lista.Add(line);
				}
			}

			int broj = rand1.Next(1, lista.Count);
			foreach (string line in lista) {
				count++;
				if (count == broj) {
					pojam = line;
					break;
				}
			}

			for (int i = 0; i < pojam.Length; i++) {
				lbltrenutno.Text = lbltrenutno.Text.Insert(i, "*");
			}

			btnIgraj.Hide();
		}

		private void btnReset_Click(object sender, EventArgs e) {
			btn_UnosSlova.Enabled = true;
			lblBroj.Text = "3";
			int broj = rand1.Next(1, 7);
			foreach (string line in lista) {
				count++;
				if (count == broj) {
					pojam = line;
					break;
				}
			}

			for (int i = 0; i < pojam.Length; i++) {
				lbltrenutno.Text = lbltrenutno.Text.Remove(i, 1);
				lbltrenutno.Text = lbltrenutno.Text.Insert(i, "*");
			}

		}

		private void btn_UnosSlova_Click(object sender, EventArgs e) {

			bool flag = false;
			bool pobjeda = false;
			if (tbSlovo.Text == "") {
				MessageBox.Show("UNESI SLOVO!", "GRESKA!");
			} else {
				char slovo = Convert.ToChar(tbSlovo.Text);
				for (int i = 0; i < pojam.Length; i++) {
					if (pojam[i] == slovo) {
						lbltrenutno.Text = lbltrenutno.Text.Remove(i, 1);
						lbltrenutno.Text = lbltrenutno.Text.Insert(i, slovo.ToString());
						flag = true;
					}
				}
				if (!flag) {
					int broj = Convert.ToInt32(lblBroj.Text);
					broj--;
					lblBroj.Text = broj.ToString();
					if (broj == 0) {
						lbltrenutno.Text = pojam;

						MessageBox.Show("Izgubili ste! Gasim program!", "REZULTAT");
						Application.Exit();
					}
				}

				tbSlovo.Clear();

				for (int i = 0; i < lbltrenutno.Text.Length; i++) {
					if (lbltrenutno.Text.Contains('*')) {
						pobjeda = false;
						break;
					}
					pobjeda = true;
				}
				if (pobjeda) {
					MessageBox.Show("Pobjedili ste sa preostalih " + lblBroj.Text + " pokušaja, gasim program!", "REZULTAT");
					Application.Exit();
				}
			}
		}
		private void btnPogodi_Click(object sender, EventArgs e) {
			int broj2 = Convert.ToUInt16(lblBroj.Text);
			if (pojam == txtboxRijec.Text) {
				lbltrenutno.Text = pojam;

				MessageBox.Show("Pobjedili ste sa preostalih " + lblBroj.Text + " pokušaja, gasim program!", "REZULTAT");
				Application.Exit();
			} else {
				broj2--;
				txtboxRijec.Text = "";
				if (broj2 < 1) {
					lblBroj.Text = "0";
					MessageBox.Show("Izgubili ste! Gasim program!", "REZULTAT");
					Application.Exit();
				}
				lblBroj.Text = broj2.ToString();
			}
		}
	}
}
