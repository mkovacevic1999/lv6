﻿namespace Vješala
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnIzlaz = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            this.btnIgraj = new System.Windows.Forms.Button();
            this.btn_UnosSlova = new System.Windows.Forms.Button();
            this.tbSlovo = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblBroj = new System.Windows.Forms.Label();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.lbl1 = new System.Windows.Forms.Label();
            this.lbltrenutno = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtboxRijec = new System.Windows.Forms.TextBox();
            this.btnPogodi = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnIzlaz
            // 
            this.btnIzlaz.Location = new System.Drawing.Point(673, 392);
            this.btnIzlaz.Name = "btnIzlaz";
            this.btnIzlaz.Size = new System.Drawing.Size(100, 33);
            this.btnIzlaz.TabIndex = 0;
            this.btnIzlaz.Text = "Izlaz";
            this.btnIzlaz.UseVisualStyleBackColor = true;
            this.btnIzlaz.Click += new System.EventHandler(this.btnIzlaz_Click);
            // 
            // btnReset
            // 
            this.btnReset.Location = new System.Drawing.Point(71, 313);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(144, 66);
            this.btnReset.TabIndex = 1;
            this.btnReset.Text = "Ponovo!";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // btnIgraj
            // 
            this.btnIgraj.Location = new System.Drawing.Point(256, 313);
            this.btnIgraj.Name = "btnIgraj";
            this.btnIgraj.Size = new System.Drawing.Size(160, 66);
            this.btnIgraj.TabIndex = 2;
            this.btnIgraj.Text = "Igraj!";
            this.btnIgraj.UseVisualStyleBackColor = true;
            this.btnIgraj.Click += new System.EventHandler(this.btnIgraj_Click);
            // 
            // btn_UnosSlova
            // 
            this.btn_UnosSlova.Location = new System.Drawing.Point(27, 31);
            this.btn_UnosSlova.Name = "btn_UnosSlova";
            this.btn_UnosSlova.Size = new System.Drawing.Size(137, 61);
            this.btn_UnosSlova.TabIndex = 4;
            this.btn_UnosSlova.Text = "Unesi slovo";
            this.btn_UnosSlova.UseVisualStyleBackColor = true;
            this.btn_UnosSlova.Click += new System.EventHandler(this.btn_UnosSlova_Click);
            // 
            // tbSlovo
            // 
            this.tbSlovo.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbSlovo.Location = new System.Drawing.Point(192, 40);
            this.tbSlovo.Name = "tbSlovo";
            this.tbSlovo.Size = new System.Drawing.Size(61, 38);
            this.tbSlovo.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(23, 109);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(148, 20);
            this.label1.TabIndex = 6;
            this.label1.Text = "Preostalo pokušaja:";
            // 
            // lblBroj
            // 
            this.lblBroj.AutoSize = true;
            this.lblBroj.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblBroj.Location = new System.Drawing.Point(188, 109);
            this.lblBroj.Name = "lblBroj";
            this.lblBroj.Size = new System.Drawing.Size(51, 20);
            this.lblBroj.TabIndex = 7;
            this.lblBroj.Text = "label2";
            // 
            // lbl1
            // 
            this.lbl1.AutoSize = true;
            this.lbl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lbl1.Location = new System.Drawing.Point(52, 227);
            this.lbl1.Name = "lbl1";
            this.lbl1.Size = new System.Drawing.Size(0, 25);
            this.lbl1.TabIndex = 8;
            // 
            // lbltrenutno
            // 
            this.lbltrenutno.AutoSize = true;
            this.lbltrenutno.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lbltrenutno.Location = new System.Drawing.Point(265, 181);
            this.lbltrenutno.Name = "lbltrenutno";
            this.lbltrenutno.Size = new System.Drawing.Size(0, 46);
            this.lbltrenutno.TabIndex = 15;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(341, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 20);
            this.label2.TabIndex = 16;
            this.label2.Text = "Pogodi riječ:";
            // 
            // txtboxRijec
            // 
            this.txtboxRijec.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.txtboxRijec.Location = new System.Drawing.Point(455, 52);
            this.txtboxRijec.Name = "txtboxRijec";
            this.txtboxRijec.Size = new System.Drawing.Size(151, 26);
            this.txtboxRijec.TabIndex = 17;
            // 
            // btnPogodi
            // 
            this.btnPogodi.Location = new System.Drawing.Point(628, 45);
            this.btnPogodi.Name = "btnPogodi";
            this.btnPogodi.Size = new System.Drawing.Size(84, 39);
            this.btnPogodi.TabIndex = 18;
            this.btnPogodi.Text = "Pogodi";
            this.btnPogodi.UseVisualStyleBackColor = true;
            this.btnPogodi.Click += new System.EventHandler(this.btnPogodi_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnPogodi);
            this.Controls.Add(this.txtboxRijec);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lbltrenutno);
            this.Controls.Add(this.lbl1);
            this.Controls.Add(this.lblBroj);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbSlovo);
            this.Controls.Add(this.btn_UnosSlova);
            this.Controls.Add(this.btnIgraj);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.btnIzlaz);
            this.Name = "Form1";
            this.Text = "Vješala";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnIzlaz;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Button btnIgraj;
        private System.Windows.Forms.Button btn_UnosSlova;
        private System.Windows.Forms.TextBox tbSlovo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblBroj;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.Label lbl1;
        private System.Windows.Forms.Label lbltrenutno;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtboxRijec;
        private System.Windows.Forms.Button btnPogodi;
    }
}

